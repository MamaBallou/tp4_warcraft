﻿using System;

namespace Unite
{
    public class Peasant
    {
        public string name;
        public string type;
        public string race;
        public string faction;
        public int hit_point;
        public int armor;

        public Peasant(string name)
        {
            this.name = name;
            this.type = "Peasant";
            this.race = "Human";
            this.faction = "Alliance";
            this.hit_point = 30;
            this.armor = 0;
        }

        public string sayHello()
        {
            return string.Format("Salut je m'appelle {0} et je suis un {1}.", this.name, this.type);
        }

        public string grunt()
        {
            return "Hmrr";
        }

        public string talk()
        {
            return "Salut, tu veux quoi ?";
        }

        public string talkToPeasant(Peasant peasant)
        {
            return string.Format("Salut mon ami, comment te nomme tu ? \nMoi ? {0}.", peasant.name);
        }

        public string talkToPeon(Peon peon)
        {
            return string.Format("Hey toi là ! Dégage de mon champ !\n{0} faire ce qu'il veut !", peon.name);
        }
    }

    public class Peon
    {
        public string name;
        public string type;
        public string race;
        public string faction;
        public int hit_point;
        public int armor;

        public Peon(string name)
        {
            this.name = name;
            this.type = "Peon";
            this.race = "Orc";
            this.faction = "Horde";
            this.hit_point = 30;
            this.armor = 0;
        }

        public string sayHello()
        {
            return string.Format("Moi {0}, moi {1}.", this.name, this.type);
        }

        public string grunt()
        {
            return "Grroooo";
        }

        public string talk()
        {
            return string.Format("{0} pas parler aux inconnus.", this.name);
        }

        public string talkToPeasant(Peasant peasant)
        {
            return "Toi qui ?!\nAhh ! Au secours !";
        }

        public string talkToPeon(Peon peon)
        {
            return string.Format("Hé ! {0} vouloir ta chaussette !\nNon ! Ca être la chaussette de {1} !", this.name, peon.name);
        }
    }
}