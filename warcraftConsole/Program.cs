﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace warcraftConsole
{
    class Program
    {
        static string setName()
        {
            Console.Write("Donnez lui un nom : ");
            string name = Console.ReadLine();
            return name;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Créer un Peasant :");
            string name = setName();
            Unite.Peasant peasant = new Unite.Peasant(name);

            Console.WriteLine("Créer un Peon :");
            name = setName();
            Unite.Peon peon = new Unite.Peon(name);

            Console.WriteLine(peasant.sayHello());
            Console.WriteLine(peon.sayHello());
            Console.WriteLine(peon.grunt());
            Console.WriteLine(peasant.talk());
            Console.WriteLine(peon.talk());
            Console.WriteLine(peasant.grunt());
            Console.WriteLine(peasant.talkToPeasant(peasant));
            Console.WriteLine(peasant.talkToPeon(peon));
            Console.WriteLine(peon.talkToPeasant(peasant));
            Console.WriteLine(peon.talkToPeon(peon));
            Console.ReadKey();
        }
    }
}
