﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Unite;

namespace UnitTestProject1
{
    [TestClass]
    public class UniteTest
    {
        [TestMethod]
        public void TestUnitePeasantConstructor()
        {
            Peasant peasant = new Peasant("Barnabé");
            Assert.AreEqual(peasant.name, "Barnabé");
            Assert.AreEqual(peasant.type, "Peasant");
            Assert.AreEqual(peasant.race, "Human");
            Assert.AreEqual(peasant.faction, "Alliance");
            Assert.AreEqual(peasant.hit_point, 30);
            Assert.AreEqual(peasant.armor, 0);
        }

        [TestMethod]
        public void TestUnitePeonConstructor()
        {
            Peon peon = new Peon("José");
            Assert.AreEqual(peon.name, "José");
            Assert.AreEqual(peon.type, "Peon");
            Assert.AreEqual(peon.race, "Orc");
            Assert.AreEqual(peon.faction, "Horde");
            Assert.AreEqual(peon.hit_point, 30);
            Assert.AreEqual(peon.armor, 0);
        }

        [TestMethod]
        public void TestPeasantSayHello()
        {
            Peasant peasant = new Peasant("Barnabé");
            Assert.AreEqual(peasant.sayHello(), "Salut je m'appelle Barnabé et je suis un Peasant.");
        }

        [TestMethod]
        public void TestPeontSayHello()
        {
            Peon peon = new Peon("José");
            Assert.AreEqual(peon.sayHello(), "Moi José, moi Peon.");
        }

        [TestMethod]
        public void TestPeasantGrunt()
        {
            Peasant peasant = new Peasant("Barnabé");
            Assert.AreEqual(peasant.grunt(), "Hmrr");
        }

        [TestMethod]
        public void TestPeonGrunt()
        {
            Peon peon = new Peon("José");
            Assert.AreEqual(peon.grunt(), "Grroooo");
        }

        [TestMethod]
        public void TestPeasantTalk()
        {
            Peasant peasant = new Peasant("Barnabé");
            Assert.AreEqual(peasant.talk(), "Salut, tu veux quoi ?");
        }

        [TestMethod]
        public void TestPeonTalk()
        {
            Peon peon = new Peon("José");
            Assert.AreEqual(peon.talk(), "José pas parler aux inconnus.");

        }

        [TestMethod]
        public void TestPeasantTalkToPeasant()
        {
            Peasant peasant = new Peasant("Barnabé");
            Assert.AreEqual(peasant.talkToPeasant(peasant), "Salut mon ami, comment te nomme tu ? \nMoi ? Barnabé.");
        }

        [TestMethod]
        public void TestPeonTalkToPeasant()
        {
            Peasant peasant = new Peasant("Barnabé");
            Peon peon = new Peon("José");
            Assert.AreEqual(peon.talkToPeasant(peasant), "Toi qui ?!\nAhh ! Au secours !");
        }

        [TestMethod]
        public void TestPeasantTalkToPeon()
        {
            Peasant peasant = new Peasant("Barnabé");
            Peon peon = new Peon("José");
            Assert.AreEqual(peasant.talkToPeon(peon), "Hey toi là ! Dégage de mon champ !\nJosé faire ce qu'il veut !");
        }

        [TestMethod]
        public void TestPeonTalkToPeon()
        {
            Peon peon = new Peon("José");
            Assert.AreEqual(peon.talkToPeon(peon), "Hé ! José vouloir ta chaussette !\nNon ! Ca être la chaussette de José !");
        }
    }
}
